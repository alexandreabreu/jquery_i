$(function(){
	$('.animated').hover( function(){
		$(this).animate({fontSize: '+=5px'},'slow');		
	}, function(){
		$(this).animate({fontSize: '-=5px'},'slow');
	});

	$('.invisible').hover(function(){
		$(this).animate({opacity:0.3},'fast');
	}, function(){
		$(this).animate({opacity:1},'fast');
	});	
});
