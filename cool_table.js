$(function(){
	$('#zombies tbody tr:even').addClass('zebra');
	$('#zombies tbody tr').hover(
		function(){
			if(!$(this).hasClass('kindagreen')) {			
				$(this).toggleClass('kindadarkred selected');
			};
		}
	);
	$('#zombies tbody tr').click(function(){
		$(this).toggleClass('kindadarkred');
		$(this).toggleClass('kindagreen');
	});
});
