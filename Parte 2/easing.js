$(function(){
		$('.color').toggle( 
			function(){
				$(this).animate({'height': '+=150px'}, 800, 'easeOutBounce').animate({backgroundColor: '#D1C087', color: '#000000'}, 1000);			
			},
			function(){
				$(this).animate({'height': '-=150px'}, 800, 'easeOutBounce').animate({backgroundColor: '#8C4242', color: '#F2E3B6'}, 500);			
			}
		);
		$('.boxed').toggle( 
			function(){
				$(this).animate({'left': '95%'}, 500, 'easeOutBack');			
			},
			function(){
				$(this).animate({'left': '0%'}, 500, 'easeOutBack');			
			}
		);
		$('.boxed').hover( 
			function(){
				$(this).animate({'font-size': '+=20px', 'height': '+=40', 'width': '+=40'}, 500, 'easeInElastic');			
			},
			function(){
				$(this).animate({'font-size': '-=20px', 'height': '-=40', 'width': '-=40'}, 500, 'easeOutElastic');			
			}
		);
});
