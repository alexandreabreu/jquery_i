$(function(){
	var sounds = ["OOOOOOOuuuuuuuuchhhh!!!!","AAAAAAAAAAAAAHHHHHHHHHHhhhhhhhhhhhhhhhhhhhhhhh!", "... urrrg... rrhrh....", "UUUUUUOOOOOOOOOOOOOOAAAAAHH!!!"];
	$('#addColor').click( function(){
		var sound = sounds[Math.floor(Math.random()*sounds.length)];
		$('<p>' + sound + '</p>').addClass("color kindagreen").insertAfter('#decrease');		
	});
	$('#increase').click( function(){		
		$('.color').animate({fontSize: '+=2px'}, 200);
	});
	$('#decrease').click( function(){		
		$('.color').animate({fontSize: '-=2px'}, 200);
	});
	$('#clean').click( function(){
		$('.color').fadeOut('normal');	
	});
	$('#stripe_it').click( function(){
		$('.color:even').animate({backgroundColor: 'rgb(45,45,45)'});	
	});
});
