$(function(){

		var restoreLeft = function() {
				$(this).appendTo('#lefty').delay(500).slideDown('slow', 'easeOutElastic');				
				};

		var restoreTop = function() {
				$(this).css('font-size', '-=120px')
				$(this).appendTo('#top');
				$(this).fadeIn('slow');
				};	

		$('.boxed').click( 
			function(){
				$(this).animate({'height':'toggle',
						 'padding-top':'toggle',
						 'padding-bottom':'toggle'
						},'slow', 'easeInElastic',restoreLeft);
			}
		);
		
		$('.tag').toggle(
			function(){
				$(this).appendTo('#righty').animate({'font-size' : '+=100px'},'slow' ,'easeOutBounce');
	
			}, function(){
				$(this).animate({'font-size' : '+=20px','opacity':'toggle'},'fast' ,'easeInCirc', restoreTop);
			});
});
