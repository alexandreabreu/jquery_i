$(function(){
	var colorClasses = ["kindared","kindagreen","kindayellow","kindadarkred"];
	$('#hideFirst').hide();

	$('#addColor').click( function(){
		var randomColor = colorClasses[Math.floor(Math.random()*colorClasses.length)];
		$('<p>Depois de um rock vem sempre outro rock!</p>').addClass("color " + randomColor).insertAfter('#hideFirst');
		
		$('#hideFirst').show();
	});
	
	$('#cleanThisMess').click( function(){
		$('.color').remove();
		$('#hideFirst').hide();
	});
	
	$('#hideFirst').click( function(){
		$('.color:first').fadeOut('800');
		$(this).hide();
	});

	var niceClean = function(){
		if ($('.color:first').is(':hidden')){
			$(this).remove();
		};
		if ($('.color').length > 0) {	
			$('.color:first').fadeOut('normal', niceClean);
		};
	};

	$('#cascade').click( function() {
		niceClean();
	});	
});
